from django.contrib.auth.models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username","password","last_name","first_name","email",)
        extra_kwargs = {'password': {'write_only': True}, "email": { 'required': True }, "first_name": { 'required': True }, "last_name": { 'required': True } }

    def create(self,validated_data):
    	user = User(**validated_data)
    	user.set_password(validated_data['password'])
    	user.save()
    	return user
