from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
	path('profile', views.UserProfile.as_view()),
    path('register', views.UserRegister.as_view()),
    path('auth', views.UserAuth.as_view()),
]