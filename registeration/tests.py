from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.test import TestCase
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory
from .serializers import UserSerializer
from .views import UserRegister, UserProfile, UserAuth

userData1 = { 'username' : 'user1', 'email' : 'user1@hello.com', 'password' : 'password', 'first_name' : 'John', 'last_name' : 'Doe' }
userData2 = { 'username' : 'user2', 'password' : 'password', 'first_name' : 'John', 'last_name' : 'Doe' }
userData3 = { 'username' : 'user3', 'email' : 'user1hello.com', 'password' : 'password', 'first_name' : 'John', 'last_name' : 'Doe' }
userData4 = { 'username' : 'user4', 'email' : 'user1@hello.com', 'password' : 'password', 'first_name' : 'John', 'last_name' : 'Doe' }

class RegistrationTest(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.user = User(**userData1)
        self.user.set_password(userData1['password'])
        self.user.save()
        self.user.auth_token = Token.objects.create(user=self.user)

    def test_registeration(self):
    	
    	request = self.factory.post('/user/register', userData1)
    	response = UserRegister.as_view()(request)
    	self.assertEqual(response.status_code, 400)

    	request = self.factory.post('/user/register', userData2)
    	response = UserRegister.as_view()(request)
    	self.assertEqual(response.status_code, 400)


    	request = self.factory.post('/user/register', userData3)
    	response = UserRegister.as_view()(request)
    	self.assertEqual(response.status_code, 400)


    	request = self.factory.post('/user/register', userData4)
    	response = UserRegister.as_view()(request)
    	self.assertEqual(response.status_code, 201)
    	self.assertEqual(response.data, {"success":True})


    def test_login(self):
    	request = self.factory.post('/user/auth', userData1)
    	response = UserAuth.as_view()(request)
    	self.assertEqual(response.status_code, 200)
    	self.assertIn("token", response.data)
    	token = response.data["token"]
    	self.assertIsInstance(token, str)


    	request = self.factory.post('/user/auth', {'username':'user1','password':'wrong_password'})
    	response = UserAuth.as_view()(request)
    	self.assertEqual(response.status_code, 400)

    	request = self.factory.post('/user/auth', {})
    	response = UserAuth.as_view()(request)
    	self.assertEqual(response.status_code, 400)

    def test_logout(self):

    	request = self.factory.delete('/user/profile')
    	force_authenticate(request, user=self.user,token=self.user.auth_token)
    	response = UserAuth.as_view()(request)
    	self.assertEqual(response.status_code, 200)
    	self.assertEqual(response.data, {"success":True})

    def test_profile(self):
    	request = self.factory.get('/user/profile')
    	response = UserProfile.as_view()(request)
    	self.assertEqual(response.status_code, 401)

    	request = self.factory.get('/user/profile')
    	force_authenticate(request, user=self.user,token=self.user.auth_token)
    	response = UserProfile.as_view()(request)
    	self.assertEqual(response.status_code, 200)
    	self.assertEqual(response.data, UserSerializer(self.user).data)

