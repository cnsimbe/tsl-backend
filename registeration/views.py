from django.contrib.auth.models import User
from registeration.serializers import UserSerializer
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from django.core.mail import send_mail
from django.template.loader import render_to_string

class UserRegister(generics.CreateAPIView):
    serializer_class = UserSerializer

    def post(self,request):
    	SendWelcomeEmail(super().post(request).data)
    	return Response({"success":True},status=201)


class UserProfile(generics.RetrieveAPIView):
	serializer_class = UserSerializer
	permission_classes = (IsAuthenticated,)

	def get_object(self):
		return self.request.user

class UserAuth(ObtainAuthToken,generics.DestroyAPIView):

	def delete(self,request, *args, **kwargs):
		request.auth.delete()
		return Response({"success":True})


def SendWelcomeEmail(userData):
	message = render_to_string('registeration/welcome_email.html', userData)
	return send_mail('Welcome to the Wall App', message, 'TSL-WallApp@tsl.io', [ userData.get('email')] , fail_silently=False, html_message=message)