from .models import WallMessage
from wall.serializers import MessageSerializer
from rest_framework import mixins, generics
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.parsers import MultiPartParser

class WallMessageView(generics.ListCreateAPIView):
	queryset = WallMessage.objects.select_related('user').order_by('-created').all()
	permission_classes = (IsAuthenticatedOrReadOnly,)
	serializer_class = MessageSerializer
	parser_classes = (MultiPartParser,)

	def perform_create(self, serializer):
		return serializer.save(user=self.request.user)