from django.contrib.auth.models import User
from rest_framework import serializers
from registeration.serializers import UserSerializer
from .models import WallMessage

class MessageSerializer(serializers.ModelSerializer):
	user = UserSerializer(read_only=True)
	text_message = serializers.CharField(allow_blank=False,required=False)
	class Meta:
		model = WallMessage
		fields = "__all__"
		depth = 1

	def validate(self,data):
		data = super().validate(data)
		if data.get("text_message", None) is None and data.get("picture", None) is None:
			raise serializers.ValidationError("Either Wall message or picture required")
		return data