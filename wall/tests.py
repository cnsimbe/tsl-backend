from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.test import TestCase
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory
from .serializers import MessageSerializer
from .views import WallMessageView
from .models import WallMessage
from django.conf import settings
import tempfile, os, shutil

userData1 = { 'username' : 'user1', 'email' : 'user1@hello.com', 'password' : 'password', 'first_name' : 'John', 'last_name' : 'Doe' }

class WallMessageTest(TestCase):

    @classmethod
    def setUpClass(self):
        super().setUpClass()
        self.upload_directory = tempfile.mkdtemp()
        settings.MEDIA_ROOT = self.upload_directory
        self.factory = APIRequestFactory()
        self.user = User(**userData1)
        self.user.set_password(userData1['password'])
        self.user.save()
        self.user.auth_token = Token.objects.create(user=self.user)
        self.test_image = open("{0}/{1}".format(os.path.dirname(__file__),'test_image.png'), 'rb')

    def test_create_messages(self):
        request = self.factory.post('/wall/messages',{'text_message':'Hello World'})
        response = WallMessageView.as_view()(request)
        self.assertEqual(response.status_code, 401)

        request = self.factory.post('/wall/messages',{})
        force_authenticate(request, user=self.user,token=self.user.auth_token)
        response = WallMessageView.as_view()(request)
        self.assertEqual(response.status_code, 400)

        request = self.factory.post('/wall/messages',{'text_message':''})
        force_authenticate(request, user=self.user,token=self.user.auth_token)
        response = WallMessageView.as_view()(request)
        self.assertEqual(response.status_code, 400)

        request = self.factory.post('/wall/messages',{'picture':''})
        force_authenticate(request, user=self.user,token=self.user.auth_token)
        response = WallMessageView.as_view()(request)
        self.assertEqual(response.status_code, 400)

        self.test_image.seek(0)
        request = self.factory.post('/wall/messages',{'picture':self.test_image})
        force_authenticate(request, user=self.user,token=self.user.auth_token)
        response = WallMessageView.as_view()(request)
        self.assertEqual(response.status_code, 201)

        request = self.factory.post('/wall/messages',{'text_message':'Hello World'})
        force_authenticate(request, user=self.user,token=self.user.auth_token)
        response = WallMessageView.as_view()(request)
        self.assertEqual(response.status_code, 201)

        self.test_image.seek(0)
        request = self.factory.post('/wall/messages',{'text_message':'Hello World','picture':self.test_image})
        force_authenticate(request, user=self.user,token=self.user.auth_token)
        response = WallMessageView.as_view()(request)
        self.assertEqual(response.status_code, 201)


    def test_get_messages(self):
        
        request = self.factory.get('/wall/messages')
        response = WallMessageView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertIn("next", response.data)
        self.assertIn("previous", response.data)
        self.assertIn("count", response.data)
        self.assertIn("results", response.data)

    @classmethod
    def tearDownClass(self):
        shutil.rmtree(self.upload_directory)
        self.test_image.close()
        super().tearDownClass()




