from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class WallMessage(models.Model):
	text_message = models.TextField(null=True,blank=True)
	picture = models.ImageField(null=True,blank=True)
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	created = models.DateTimeField(auto_now_add=True)