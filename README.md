# TSL Tech Assignment

## Prerequisites

This project requires Python 3 and VirtualEnv

A SENDGRID_API_KEY is also needs for sending the Welcome Email


## Installing

Create and use a virtual env

```
virtualenv tsl-env --python=/usr/bin/python3
source tsl-env/bin/activate 
```


Clone the repository and install pip requirements

```
git clone https://gitlab.com/cnsimbe/tsl-backend.git
cd tsl-backend
pip install -r requirements.txt
```


## Running

Set your SENDGRID_API_KEY in the environment

```
export SENDGRID_API_KEY=your_api_key
```


Start the runserver

```
python manage.py runserver
```


## Testing

```
python manage.py test
```



## Built With

* [Django](https://www.djangoproject.com/)